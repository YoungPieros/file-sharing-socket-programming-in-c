done: server client

server: server.c socketLibrary.o
	gcc server.c -o server
client: client.c socketLibrary.o
	gcc client.c -o client
socketLibrary.o: socketLibrary.h
	gcc socketLibrary.h -o socketLibrary.o


clean:
	rm server && rm client && rm socketLibrary.o
