#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>


#define HEARTBEATPERIOD 2
#define UPLOAD "upload"
#define DOWNLOAD "download"
#define SERVERDIRECTORY "./ServerFolder/"
#define CLIENTDIRECTORY "./ClientFolder/"
#define FINISHUPLOADMESSAGE "`"
#define FOUNDFILEINSERVER " found in server"
#define NOTFOUNDFILEINSERVER " not found in server"
#define REQUEST_FILE_FLAG "1"
#define SEND_FILE_FLAG "0"
#define MAX_CLIENTS 5
#define MAXCLIENT 10
#define BUFFER_SIZE 1024
#define TRUE 1
#define FALSE 0

void splitCommand (char *strcommand, char *command, char *filename) {
	command[0] = '\0';
	filename[0] = '\0';
	int commandCounter = 0;
	int strCounter = 0;
	int strLength = 0;
	int commandLength = strlen(strcommand);
	while (strCounter < commandLength) {
		if (strcommand[strCounter] == ' ') {
			if (commandCounter == 0) {
				commandCounter++;
				command[strLength] = '\0';
			}
			strLength = 0;
		}
		else if (strcommand[strCounter] == '\n') {
			if (commandCounter == 1)
				filename[strLength] = '\0';
			break;
		}
		else {
			if (commandCounter == 0)
				command[strLength] = strcommand[strCounter];
			else
				filename[strLength] = strcommand[strCounter];
			strLength++;
		}
		strCounter++;
	}
}

void setTCPServerSelectTime (struct timeval *timer) {
	timer->tv_sec = 0.2;
	timer->tv_usec = 0;
}

void logMessage (char *message) {
    write (1, message, strlen(message));
}

void getServerFileAddress (char *filename, char* address) {
    strcpy (address, SERVERDIRECTORY);
    strcat (address, filename);
}

void getClientFileAddress (char *filename, char* address, char *port) {
    struct stat st = {0};
    strcpy (address, CLIENTDIRECTORY);
    strcat (address, port);
    if (stat(address, &st)) 
        mkdir (address, 0700);
    strcat (address, "/");
    strcat (address, filename);
}

int strToInt (char *str) {
    int number = 0;
    for (int i = 0; str[i] != '\0'; i++)
        number = number * 10 + (str[i] - '0');
    return number;
}

void parseUDPMessage (char *str, char *file, char *port, char *ReqORRes) {
    int i = 0, PorF = 0, length = 0;
    for (i = 0; str[i] != '\0'; i++) {
        if (PorF == 0 && str[i] != ' ') {
            file[length] = str[i];
            length++;
        }
        else if (PorF == 0 && str[i] == ' ') {
            file[length] = '\0';
            length = 0;
            PorF = 1;
        }
        else if (PorF == 1 && str[i] != ' ') {
            port[length] = str[i];
            length++;
        }
        else if (PorF == 1 && str[i] == ' ') {
            port[length] = '\0';
            length = 0;
            PorF = 2;
        }
        else {
            ReqORRes[length] = str[i];
            length++;
        }
    }
    ReqORRes[length] = '\0';
}
