#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h>
#include <strings.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "socketLibrary.h"
#include "ConnectionClient.h"


struct HeartbeatClient {
	int socketfd;
	struct sockaddr_in servaddr;
	int isServerOn;
	char message[BUFFER_SIZE];
	int reuseOption;
	int tcpServerSocket;
}
heartbeatClient;

struct TCPClient {
	int socket;
	struct sockaddr_in servaddr;
	char message[BUFFER_SIZE];
	char filename[128];
}
tcpClient;

struct FileClient {
	int filedescriptor;
	int state;
	char filename[128];
};

struct ClientPTP {
	int selfSocket;
	int selfSocket2;
	int commonSocket;
	char selfPort[128];
	char commonPort[128];
	struct sockaddr_in servaddr;
	struct sockaddr_in servaddr2;
	struct sockaddr_in clientAddress;
	struct sockaddr_in commonaddr;
	fd_set fds;
	int maxfd;
	int clientsFDs[MAX_CLIENTS];
	struct ConnectionClient otherClients[MAX_CLIENTS];
	struct FileClient fileClients[MAX_CLIENTS];
	char broadcastMessage[128];
	int selfRequestFile;
	int otherRequestFile;
	struct timeval selectTime;
	int downloadFD;
	char fileRequested[128];
}
ptpClient;
