#include "Server.h"

void sendNotFoundFileMessage (char *filename, int connectionFD) {
	char message[256];
	strcpy (message, filename);
	strcat (message, NOTFOUNDFILEINSERVER);
	write (connectionFD, message, sizeof(message));
}

void sendFoundFileMessage (char *filename, int connectionFD) {
	char message[256];
	strcpy (message, filename);
	strcat (message, FOUNDFILEINSERVER);
	write (connectionFD, message, sizeof(message));
}

void checkBeingFileInDB (struct ConnectionClient *client, int connectionFD, char *buffer) {
	char addressFile[128];
	getServerFileAddress (buffer, addressFile);
	if (access (addressFile, R_OK) == -1) {
		resetConnectionClient (client);
		sendNotFoundFileMessage (buffer, connectionFD);
		logMessage (buffer);
		logMessage (" not found in server\n\n");
	}
	else {
		logMessage (buffer);
		logMessage (" found in server\ndownloading starts\n");
		sendFoundFileMessage(buffer, connectionFD);
		client->connectionFD = connectionFD;
		client->state = 1;
		client->filedescriptor = open(addressFile, O_RDONLY);
	}
}

void sendDownloadingFileData (struct ConnectionClient *client, int connectionFD, char *buffer) {
	int readBytes;
	char databuffer[BUFFER_SIZE];
	bzero(&databuffer, sizeof(databuffer));
	readBytes = read (client->filedescriptor, databuffer, sizeof(databuffer));
	databuffer[strlen(databuffer)] = '\0';
	if (strlen(databuffer) == 0) {
		bzero (&databuffer, sizeof(databuffer));
		strcpy (databuffer, FINISHUPLOADMESSAGE);
		write (connectionFD, databuffer, sizeof(databuffer));
		close (client->filedescriptor);
		resetConnectionClient(client);
		logMessage ("sending data is finished succsesfully\n\n");
		return;
	}
	logMessage ("sending data file ...\n");
	write (connectionFD, databuffer, sizeof(databuffer));
}

void runDownloadCommand (struct ConnectionClient *client, int connectionFD, char *buffer) {
	if (client->state == 0)
		checkBeingFileInDB (client, connectionFD, buffer);
	else if (client->state == 1)
		sendDownloadingFileData (client, connectionFD, buffer);
}

void runUploadCommand (struct ConnectionClient *client, int connectionFD, char *buffer) {
	char addressFile[128];
	char message[128];
	int readBytes;
	strcpy (message, "feedback");
	if (client->state == 0) {
		getServerFileAddress (buffer, addressFile);
		unlink(addressFile);
		client->filedescriptor = open(addressFile, O_CREAT | O_WRONLY , S_IRUSR | S_IWUSR);
		client->state = 1;
		logMessage ("creating data was successfully in server\n");
	}
	else if (client->state == 1 || client->state == 2) {
		if (strlen(buffer) == 1 && strcmp(buffer, FINISHUPLOADMESSAGE) == 0) {
			close (client->filedescriptor);
			resetConnectionClient(client);
			logMessage ("uploading file was successfully\n");
		}
		else {
			logMessage ("uploading file ...\n");
			write(client->filedescriptor, buffer, strlen(buffer));
		}

	}
	send (connectionFD, message, sizeof(message), 0);
}

void comunicateWithClient (struct ConnectionClient *client, int connectionFD, char *buffer) {
	char feedback[128];
	strcpy (feedback, "feedback");
	if (client->request == 0) {
		if (strcmp(buffer, DOWNLOAD) == 0)
			client->request = 1;
		else if (strcmp(buffer, UPLOAD) == 0) {
			logMessage ("uploading data is started\n");
			client->request = 2;
		}	
		else
			return;
		client->state = 0;
		write(connectionFD, feedback, sizeof(feedback));
	}
	else if (client->request == 1)
		runDownloadCommand (client, connectionFD, buffer);
	else if (client->request == 2)
		runUploadCommand (client, connectionFD, buffer);
}


void createTCPServer () {
	int i;
	tcpServer.socket = socket(AF_INET, SOCK_STREAM, 0); 
	bzero(&tcpServer.address, sizeof(tcpServer.address)); 
	tcpServer.address.sin_family = AF_INET; 
	tcpServer.address.sin_addr.s_addr = htonl(INADDR_ANY); 
	tcpServer.address.sin_port = htons(strToInt(TCPSERVERPORT));
	bind(tcpServer.socket, (struct sockaddr*)&tcpServer.address, sizeof(tcpServer.address)); 
	listen(tcpServer.socket, MAXCLIENT);
	FD_ZERO(&tcpServer.fds);
	tcpServer.maxfd = 1 + (tcpServer.socket > heartbeatServer.socketfd ? tcpServer.socket : heartbeatServer.socketfd);
	for (i = 0; i < MAX_CLIENTS; i++) {
		tcpServer.connectedFDs[i] = 0;
		resetConnectionClient (&tcpServer.clients[i]);
	}
	logMessage ("server is created\n");
}

void setFDsSetting () {
	int fd, i;
	FD_ZERO (&tcpServer.fds);
	FD_SET(tcpServer.socket, &tcpServer.fds);
	setTCPServerSelectTime (&tcpServer.selectTime);
	for (i = 0; i < MAX_CLIENTS; i++) {
		fd = tcpServer.connectedFDs[i];
		if (fd > 0)
			FD_SET (fd, &tcpServer.fds);
		if (tcpServer.maxfd < fd)
			tcpServer.maxfd = fd;		
	}
}

void handleConnectiongRequest () {
	int len, newConnectionFD, i;
	len = sizeof(tcpServer.clientAddress);
	if (FD_ISSET(tcpServer.socket, &tcpServer.fds)) {
		newConnectionFD = accept (tcpServer.socket, (struct sockaddr *)&tcpServer.clientAddress, &len);
		for (i = 0; i < MAX_CLIENTS; i++)
			if (tcpServer.connectedFDs[i] == 0) {
				tcpServer.connectedFDs[i] = newConnectionFD;
				break;
			}
	}
}

void handleClientsRequest () {
	char buffer[BUFFER_SIZE];
	int connectionFD, readBytes, i;
	for (i = 0; i < MAX_CLIENTS; i++) {
		connectionFD = tcpServer.connectedFDs[i];
		if (FD_ISSET(connectionFD, &tcpServer.fds)) {
			readBytes = read (connectionFD, buffer, sizeof(buffer));
			if (strlen(buffer) == 0) {
				close(connectionFD);
				tcpServer.connectedFDs[i] = 0;
			}
			comunicateWithClient (&tcpServer.clients[i], connectionFD, buffer);
		}
	}
}

void listenClients () {
	char buffer[1024];
	int len, newConnectionFD, isSocketSelected, i, connectoinFD, readBytes;
	setFDsSetting ();
	isSocketSelected = select(tcpServer.maxfd + 1, &tcpServer.fds, NULL, NULL, &tcpServer.selectTime);
	if (isSocketSelected == -1 || isSocketSelected == 0)
		return;
	handleConnectiongRequest ();
	handleClientsRequest ();
}

void initialServerOptions () {
	heartbeatServer.broadcast = 1;
	heartbeatServer.reuseport = 1;
	heartbeatServer.heartbeatCounter = 1;
}

void setServerClientSetting (int port) {
	
	memset(&heartbeatServer.servaddr, 0, sizeof(heartbeatServer.servaddr));
	memset(&heartbeatServer.cliaddr, 0, sizeof(heartbeatServer.cliaddr));

	heartbeatServer.servaddr.sin_family = AF_INET;
	heartbeatServer.servaddr.sin_addr.s_addr = inet_addr("255.255.255.255"); 
	heartbeatServer.servaddr.sin_port = htons(port);
	setsockopt(heartbeatServer.socketfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &heartbeatServer.reuseport, sizeof(heartbeatServer.reuseport));
	if (setsockopt (heartbeatServer.socketfd, SOL_SOCKET, SO_BROADCAST, &heartbeatServer.broadcast, sizeof(heartbeatServer.broadcast))) {
		logMessage ("heart beat set options is failed\n");
		exit(EXIT_FAILURE);
	}
	heartbeatServer.cliaddr = heartbeatServer.servaddr;
}

void sendHeartbeatFrequently () {
	char message[128];
	strcpy (message, TCPSERVERPORT);
	if (sendto (heartbeatServer.socketfd, message, sizeof(message), 0, (const struct sockaddr *)&heartbeatServer.cliaddr, sizeof(heartbeatServer.cliaddr)) < 0) {
		logMessage ("an error is occured in heartbeat\n");
		exit (EXIT_FAILURE);
	}
	alarm(HEARTBEATPERIOD);
}


void createHeartbeat (int port) {
	initialServerOptions();
	heartbeatServer.socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (heartbeatServer.socketfd < 0) {
		logMessage ("heart beat creating is failed\n");
		exit(EXIT_FAILURE);
	}
	setServerClientSetting(port);
}

int main(int argc, char *argv[]) { 
	if (argc != 2) {
		logMessage ("correct server command:\nserver heartbeat_X (like: server 8080)\n");
		exit (EXIT_FAILURE);
	}
	createHeartbeat (strToInt(argv[1]));
	createTCPServer ();
	signal(SIGALRM, sendHeartbeatFrequently);
	alarm(HEARTBEATPERIOD);
	while (TRUE)
		listenClients();
	return 0; 
} 
