#include "socketLibrary.h"
#include "ConnectionClient.h"


#define TCPSERVERPORT "8181" 


struct HeartbeatServer {
	int socketfd;
	struct sockaddr_in servaddr;
	struct sockaddr_in cliaddr;
	int broadcast;
	int reuseport;
	int heartbeatCounter;
}
heartbeatServer;



struct TcpServer {
	int connectedFDs[MAX_CLIENTS];
	struct ConnectionClient clients[MAX_CLIENTS];
	int socket;
	struct sockaddr_in address;
	struct sockaddr_in clientAddress;
	fd_set fds;
	struct timeval selectTime;
	int maxfd;
}
tcpServer;
