#include "Client.h"

void makeClientDirectory (char *port) {
	char address[128];
    struct stat st = {0};
    strcpy (address, CLIENTDIRECTORY);
    strcat (address, port);
    if (stat(address, &st)) 
        mkdir (address, 0700);
}

// 
void createClientPTP (char* selfClientPort, char* commonPort) {
	int i, opt = 1;
	ptpClient.selfRequestFile = 0;
	ptpClient.otherRequestFile = 0;

	ptpClient.selfSocket = socket(AF_INET, SOCK_STREAM, 0); 
	ptpClient.selfSocket2 = socket(AF_INET, SOCK_STREAM, 0); 	
	ptpClient.commonSocket = socket(AF_INET, SOCK_DGRAM, 0); 

	bzero(&ptpClient.servaddr, sizeof(ptpClient.servaddr));
	bzero(&ptpClient.commonaddr, sizeof(ptpClient.servaddr)); 

	memset(&ptpClient.commonaddr, 0, sizeof(ptpClient.servaddr));

	ptpClient.servaddr.sin_family = AF_INET; 
	ptpClient.servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	ptpClient.servaddr.sin_port = htons(strToInt(selfClientPort));

	ptpClient.servaddr2.sin_family = AF_INET; 
	ptpClient.servaddr2.sin_addr.s_addr = htonl(INADDR_ANY); 
	ptpClient.servaddr2.sin_port = htons(strToInt(selfClientPort));


	ptpClient.clientAddress.sin_family = AF_INET; 
	ptpClient.clientAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	ptpClient.commonaddr.sin_family = AF_INET;
	ptpClient.commonaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST); 
	ptpClient.commonaddr.sin_port = htons(strToInt(commonPort));

	setsockopt(ptpClient.commonSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
	setsockopt (ptpClient.commonSocket, SOL_SOCKET, SO_BROADCAST, &opt, sizeof(opt));

	strcpy (ptpClient.selfPort, selfClientPort);
	strcpy (ptpClient.commonPort, commonPort);
	makeClientDirectory (ptpClient.selfPort);

	bind(ptpClient.selfSocket, (struct sockaddr*)&ptpClient.servaddr, sizeof(ptpClient.servaddr)); 
	bind(ptpClient.commonSocket, (struct sockaddr*)&ptpClient.commonaddr, sizeof(ptpClient.commonaddr)); 

	listen(ptpClient.selfSocket, MAXCLIENT);

	FD_ZERO(&ptpClient.fds);
	ptpClient.maxfd = 1 + (ptpClient.selfSocket > ptpClient.commonSocket ? ptpClient.selfSocket : ptpClient.commonSocket);
	for (i = 0; i < MAX_CLIENTS; i++) {
		ptpClient.clientsFDs[i] = -1;
		resetConnectionClient (&ptpClient.otherClients[i]);
	}
}

// 
void setTCPClientSetting () {
	memset(&tcpClient.servaddr, 0, sizeof(tcpClient.servaddr)); 
	tcpClient.servaddr.sin_family = AF_INET; 
	tcpClient.servaddr.sin_port = htons(strToInt(heartbeatClient.message));
	tcpClient.servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
}

// 
void createTCPClient () {
	tcpClient.socket = socket(AF_INET, SOCK_STREAM, 0);
	if (tcpClient.socket < 0){
		logMessage ("connectiong to server is failed\n");
		exit(EXIT_FAILURE);
	}
	setTCPClientSetting ();
	if (connect (tcpClient.socket, (struct sockaddr *)&tcpClient.servaddr, sizeof(tcpClient.servaddr)) < 0) {
		perror ("connection to server is failed\n");
		exit(EXIT_FAILURE);
	}
}

// 
void initialHeartbeatClient () {
	heartbeatClient.reuseOption = 1;
} 

// 
void setHeartbeatClientSetting (int heartbeatPort) {
	memset(& heartbeatClient.servaddr, 0, sizeof(heartbeatClient.servaddr)); 
	heartbeatClient.servaddr.sin_family = AF_INET; 
	heartbeatClient.servaddr.sin_port = htons(heartbeatPort); 
	heartbeatClient.servaddr.sin_addr.s_addr = INADDR_ANY;
	setsockopt(heartbeatClient.socketfd, SOL_SOCKET, SO_REUSEADDR, &heartbeatClient.reuseOption, sizeof(heartbeatClient.reuseOption));	
}

// 
void createHeartbeatClient (int heartbeatPort) {
	initialHeartbeatClient();
	if ( (heartbeatClient.socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		perror ("udp socket creation failed"); 
		exit(EXIT_FAILURE); 
	} 
	setHeartbeatClientSetting(heartbeatPort);
	if ( bind(heartbeatClient.socketfd, (const struct sockaddr *)& heartbeatClient.servaddr, sizeof(heartbeatClient.servaddr)) < 0 ) { 
		perror("connecting to server is failed"); 
		exit(EXIT_FAILURE); 
	}
}

// 
void checkServerOnFrequently () {
	char *message;
	int messageLength;
	int len = sizeof (heartbeatClient.servaddr);
	messageLength = recvfrom (heartbeatClient.socketfd, heartbeatClient.message, BUFFER_SIZE, MSG_DONTWAIT, (struct sockaddr*) &heartbeatClient.servaddr, &len);
	if (messageLength == -1)
		heartbeatClient.isServerOn = 0;
	else if (heartbeatClient.isServerOn != 1) {
			createTCPClient ();
			heartbeatClient.isServerOn = 1;
	}
}

void checkRequestingFileFromOtherClients () {
	int messageLength;
	int len = sizeof(ptpClient.commonaddr);
	messageLength = recvfrom (ptpClient.commonSocket, ptpClient.broadcastMessage, sizeof(ptpClient.broadcastMessage), 
					MSG_DONTWAIT, (struct sockaddr *)&ptpClient.commonaddr, &len);
	if (messageLength == -1) {
		ptpClient.otherRequestFile = 0;
		return;
	}
	ptpClient.otherRequestFile = 1;
}

// 
int canUploadFile () {
	if (heartbeatClient.isServerOn == 0) {
		logMessage ("server is down, uploading file is failed\n");
		return 0;
	}
	return 1;	
}

// 
int getUploadedFD () {
	char fileAddress[128];
	getClientFileAddress(tcpClient.filename, fileAddress, ptpClient.selfPort);
	int fd = open(fileAddress, O_RDONLY);
	int readBytes;
	if (fd < 0) {
		logMessage ("file not found to uplaod\n");
		return 0;
	}
	return fd;
}

// 
void sendFileName () {
	char feedback[128];
	write (tcpClient.socket, tcpClient.filename, sizeof(tcpClient.filename));
	recv (tcpClient.socket, feedback, sizeof(feedback), 0);
}

// 
void sendDataFile (int fd) {
	char message[128];
	char buffer[1024];
	int readBytes;
	strcpy (message, "data file is uploading");
	while (TRUE) {
		bzero(&buffer, sizeof(buffer));
		read (fd, buffer, sizeof(buffer));
		if (strlen(buffer) == 0)
			return;
		logMessage ("data sending ...\n");
		write (tcpClient.socket, buffer, strlen(buffer));
		recv (tcpClient.socket, message, sizeof(message), 0);
	}
}

// 
void sendFinishMessage () {
	char message[32];
	char feedback[128];
	strcpy (message, FINISHUPLOADMESSAGE);
	write (tcpClient.socket, message, sizeof(message));
	recv (tcpClient.socket, feedback, sizeof(feedback), 0);
}

// 
void sendStartUploadFileMessage () {
	char message[32];
	char feedback[128];
	strcpy (message, UPLOAD);
	write (tcpClient.socket, message, sizeof(message));
	recv (tcpClient.socket, feedback, sizeof(feedback), 0);
}

// 
void uploadFile () {	
	int fd = getUploadedFD ();
	if (fd == 0 || canUploadFile() == 0)
		return;
	sendStartUploadFileMessage ();
	sendFileName ();
	logMessage ("uploading file request is send to server\n");
	sendDataFile (fd);
	sendFinishMessage ();
	logMessage ("file is uploaded succsessfully\n\n");
}

// 
void sendStartDownloadFileMessage () {
	char message[32];
	char feedback[128];
	strcpy (message, DOWNLOAD);
	write (tcpClient.socket, message, sizeof(message));
	recv (tcpClient.socket, feedback, sizeof(feedback), 0);
	logMessage ("downloading file is started from server\n");
}

// 
int isFileAvailableInServer (char *filename) {
	char feedback[128];
	char foundMessage[256];
	char message[128];
	strcpy (message, filename);
	write (tcpClient.socket, message, sizeof(message));
	strcpy (foundMessage, filename);
	strcat (foundMessage, FOUNDFILEINSERVER);
	recv (tcpClient.socket, feedback, sizeof(feedback), 0);
	if (strcmp (feedback, foundMessage) == 0)
		return 1;
	logMessage ("file not found in server, request file method well be peer to peer\n");
	return 0; 
}

// 
int canDownloadFromServer (char *filename) {
	if (heartbeatClient.isServerOn == 0) {
		logMessage ("server is down, request of file method will be peer to peer\n");
		return 0;
	}
	sendStartDownloadFileMessage();
	return isFileAvailableInServer (filename);	
}

// 
void downloadFromSocket (int socket, char *filename) {
	char message[32];
	char buffer[1024];
	char addresfile[128];
	int fd, readBytes;
	bzero(&message, sizeof(message));
	getClientFileAddress (filename, addresfile, ptpClient.selfPort);
	unlink (addresfile);
	fd = open(addresfile, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
	read (socket, buffer, sizeof(buffer));
	strcpy (message, "downloading data");
	while (TRUE) {
		bzero (&buffer, sizeof(buffer));
		write (socket, message, sizeof(message));
		read (socket, buffer, sizeof(buffer));
		if (strlen(buffer) == 1 && strcmp(buffer, FINISHUPLOADMESSAGE) == 0)
			break;
		logMessage ("data downloadin ...\n");
		write (fd, buffer, strlen(buffer));
	}
	close(fd);
	logMessage ("downloading file was succsessfully\n\n");
}

// 
void createDownloadRequestMessagePTP (char *message) {
	strcpy (ptpClient.fileRequested, tcpClient.filename);
	strcpy (message, tcpClient.filename);
	strcat (message, " ");
	strcat (message, ptpClient.selfPort);
	strcat (message, " ");
	strcat (message, REQUEST_FILE_FLAG);
}

// 
void sendBroadcastMessage () {
	char message[128];
	if (ptpClient.selfRequestFile == 0)
		return;
	createDownloadRequestMessagePTP (message);
	if (sendto (ptpClient.commonSocket, message, sizeof(message), 0, (const struct sockaddr *)&ptpClient.commonaddr , sizeof(ptpClient.commonaddr)) < 0) {
		perror ("an error is occured in sending download file to other clients\n");
	}
}

// 
void downloadFile () {
	if (canDownloadFromServer(tcpClient.filename) == 1)
		downloadFromSocket (tcpClient.socket, tcpClient.filename);
	else
		ptpClient.selfRequestFile = 1;
}

// 
void runCommand () {
	char strcommand[BUFFER_SIZE];
	char command[128];
	char filename[128];
	int readBytes;
	readBytes = read(STDIN_FILENO, strcommand, sizeof(strcommand));
	splitCommand(strcommand, command, tcpClient.filename);
	if (strcmp(command, UPLOAD) == 0)
		uploadFile();
	else if (strcmp(command, DOWNLOAD) == 0)
		downloadFile();
}

// 
void setPtpFdsSettings () {
	int fd, i;
	FD_ZERO (&ptpClient.fds);
	FD_SET(ptpClient.selfSocket, &ptpClient.fds);
	FD_SET(ptpClient.commonSocket, &ptpClient.fds);
	FD_SET(STDIN_FILENO, &ptpClient.fds);
	setTCPServerSelectTime (&ptpClient.selectTime);
	for (i = 0; i < MAX_CLIENTS; i++) {
		fd = ptpClient.clientsFDs[i];
		if (fd > 0)
			FD_SET (fd, &ptpClient.fds);
		if (ptpClient.maxfd < fd)
			ptpClient.maxfd = fd;		
	}
}

// 
void downloadFromClient (int socket, char *file) {
	char message[32];
	char buffer[1024];
	char addresfile[128];
	char hostClient[128];
	int fd, readBytes;
	bzero(&message, sizeof(message));
	bzero(&buffer, sizeof(buffer));
	bzero(&hostClient, sizeof(hostClient));

	getClientFileAddress (file, addresfile, ptpClient.selfPort);
	unlink (addresfile);
	fd = open(addresfile, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
	strcpy (message, file);
	write (socket, message, sizeof(message));
	readBytes = recv (socket, buffer, sizeof(buffer), 0);
	strcpy (hostClient, buffer);
	logMessage ("downloading file from ");
	logMessage (hostClient);
	logMessage (" is started\n");
	while (TRUE) {
		write (socket, message, sizeof(message));
		bzero (&buffer, sizeof(buffer));
		readBytes = read (socket, buffer, sizeof(buffer));
		if (strlen(buffer) == 1 && strcmp(buffer, FINISHUPLOADMESSAGE) == 0)
			break;
		logMessage ("downloading data from ");
		logMessage (hostClient);
		logMessage ("  ...\n");
		write (fd, buffer, strlen(buffer));
	}
	logMessage ("downloading file is finished succesfully\n\n");
	close(fd);
}

// 
void uploadFileFromClientToClient (struct FileClient *fileClient, int connectionFD, char *buffer) {
	char addressFile[128];
	char message[32];
	int readBytes;
	char databuffer[1024];
	strcpy (message, ptpClient.selfPort);

	if (fileClient->state == 0) {
		getClientFileAddress (buffer, addressFile, ptpClient.selfPort);
		fileClient->filedescriptor = open (addressFile, O_RDONLY);
		fileClient->state = 1;
		write (connectionFD, message, sizeof(message));
	}
	else if (fileClient->state == 1) {
		bzero(&databuffer, sizeof(databuffer));
		readBytes = read(fileClient->filedescriptor, databuffer, sizeof(databuffer));
		if (strlen(databuffer) == 0) {
			strcpy (databuffer, FINISHUPLOADMESSAGE);
			write (connectionFD, databuffer, sizeof(databuffer));
			close (fileClient->filedescriptor);
			fileClient->filedescriptor = -1;
			logMessage ("file recieving is finished succesfully\n\n");
			return;
		}
		logMessage ("data ");
		logMessage (buffer);
		logMessage (" is sending to client ...\n");
		write (connectionFD, databuffer, sizeof(databuffer));
	}
}

// 
void setFileClientSetting (struct FileClient *fileClient) {
	fileClient->state = 0;
}

// 
void handleConnectingClients () {
	int newConnectionFD, len, i;
	len = sizeof(ptpClient.clientAddress);
	newConnectionFD = accept (ptpClient.selfSocket, (struct sockaddr*)&ptpClient.clientAddress, &len);
	for (i = 0; i < MAX_CLIENTS; i++)
		if (ptpClient.clientsFDs[i] <= 0) {
			ptpClient.clientsFDs[i] = newConnectionFD;
			setFileClientSetting (&ptpClient.fileClients[i]);
			break;
		}
}

// 
void sendFindingFileMessageToClient (char *file) {
	char message[128];
	strcpy (message, file);
	strcat (message, " ");
	strcat (message, ptpClient.selfPort);
	strcat (message, " ");
	strcat (message, SEND_FILE_FLAG);
	sendto (ptpClient.commonSocket, message, sizeof(message), 0, (struct sockaddr*)&ptpClient.commonaddr, sizeof(ptpClient.commonaddr));
}

// 
void handleUDMessages () {
	int len, readBytes;
	char buffer[BUFFER_SIZE], file[128], portStr[128], ReqOrRes[32], addressFile[128];
	char message[256];
	len = sizeof(ptpClient.clientAddress);
	readBytes = recvfrom (ptpClient.commonSocket, buffer, sizeof(buffer), 0, (struct sockaddr*)&ptpClient.clientAddress, &len);
	parseUDPMessage (buffer, file, portStr, ReqOrRes);
	if (ptpClient.selfRequestFile && strcmp (ptpClient.fileRequested, file) == 0 && strcmp(ReqOrRes, SEND_FILE_FLAG) == 0) {
		ptpClient.servaddr2.sin_port = htons(strToInt(portStr));
		connect (ptpClient.selfSocket2, (struct sockaddr*)&ptpClient.servaddr2, sizeof(ptpClient.servaddr2));
		strcpy (message, "client ");
		strcat (message, portStr);
		strcat (message, " has ");
		strcat (message, file);
		strcat (message, "\ndonwloading file is started\n");
		logMessage (message);
		ptpClient.selfRequestFile = 0;
		downloadFromClient (ptpClient.selfSocket2, ptpClient.fileRequested);
	}
	else if (strcmp (ReqOrRes, REQUEST_FILE_FLAG) == 0) {
		getClientFileAddress (file, addressFile, ptpClient.selfPort);
		if (access (addressFile, F_OK) != -1 && strcmp(ptpClient.selfPort, portStr) != 0) {
			logMessage ("request downloading ");
			logMessage (file);
			logMessage (" is accepted from ");
			logMessage (portStr);
			logMessage ("\n");
			sendFindingFileMessageToClient (file);
		}
	}
}

// 
void handleRequests () {
	char buffer[BUFFER_SIZE];
	char recieveMessage[128];
	int connectionFD, i, readBytes;
	for (i = 0; i < MAX_CLIENTS; i++) {
		connectionFD = ptpClient.clientsFDs[i];
		if (connectionFD != -1 && FD_ISSET(connectionFD, &ptpClient.fds)) {
			readBytes = read (connectionFD, buffer, sizeof(buffer));
			if (strlen(buffer) == 0) {
				close(connectionFD);
				ptpClient.clientsFDs[i] = 0;
			}
			uploadFileFromClientToClient (&ptpClient.fileClients[i], connectionFD, buffer);
		}
	}
}

// 
void listenClients () {
	int isSocketSelected;
	setPtpFdsSettings ();
	isSocketSelected = select (ptpClient.maxfd + 1, &ptpClient.fds, NULL, NULL, &ptpClient.selectTime); 
	if (isSocketSelected == 0 || isSocketSelected == -1)
		return;
	if (FD_ISSET(ptpClient.selfSocket, &ptpClient.fds))
		handleConnectingClients ();
	if (FD_ISSET (ptpClient.commonSocket, &ptpClient.fds))
		handleUDMessages ();
	if (FD_ISSET (STDIN_FILENO, &ptpClient.fds))
		runCommand ();
	handleRequests ();
}

// 
void runSignalFunctions () {
	checkServerOnFrequently ();
	sendBroadcastMessage ();
	alarm(HEARTBEATPERIOD);
}

int main(int argc, char *argv[]) {
	if (argc != 4) {
		logMessage ("correct creating client command :\nclient heartbeat_port_X broadcast_port_Y client_port_M (like : client 8080 8888 8000\n");
		exit (EXIT_FAILURE);
	}
	createClientPTP (argv[3], argv[2]);
	createHeartbeatClient (strToInt(argv[1]));
	logMessage ("client is created\n");
	signal (SIGALRM, runSignalFunctions);
	alarm (HEARTBEATPERIOD);
	while (1)
		listenClients ();
	return 0; 
} 
