struct ConnectionClient {
	int filedescriptor; // open fd client for reading from or writing to
	int request; // client request : 0: no request; 1: download; 2: upload

	// client state
	// when request is 0 state is -1
	// upload: 0: request is recieved; 1: get file name; 2: receiving data;
	// download: 0: request is recieved & get file name; 1: sending data;
	int state;
	int connectionFD; // the connectionFD in tcpServer
};


void resetConnectionClient (struct ConnectionClient *client) {
	client->request = 0;
	client->state = -1;
	client->filedescriptor = -1;
}


